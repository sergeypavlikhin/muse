 /*DIALOG*/
 var videos = {
     "di": "https://www.youtube.com/embed/I5sJhSNUkwQ",
     "is": "https://www.youtube.com/embed/VXPoJAyeF8k",
     "p": "https://www.youtube.com/embed/UqLRqzTp6Rk"
 };


 $(document).on('click', '.default-state', function () {
     showDialog(this);
 });

 function closeDialog() {
     $('.dialog-video').animate({
         opacity: 0,
         zIndex: -1
     });
     $('.back-dialog').animate({
         opacity: 0,
         zIndex: -1
     });
 }

 function showDialog(elem) {
         $('.dialog-video').css("zIndex", "100");
         $('.back-dialog').css("zIndex", "99");

         $('.dialog-video').animate({
             opacity: 1

         });
         $('.back-dialog').animate({
             opacity: 1
         });

         var video = videos[$(elem).attr('data-video')];
         $('iframe').attr('src', video);
     }
     /**/
