 $(document).scroll(function () {
     /*Effect top player*/
     if (document.body.scrollTop >= 100) {
         $('.audio-player').addClass('out-player');
         console.log('>= 100');
     }
     if (document.body.scrollTop < 100) {
         console.log('<100');
         $('.audio-player').removeClass('out-player');
     }

 });

/*Widget for index.html page with three artists*/
 $('.wrapper-image img').mouseover(function () {
     $(this).attr('src', $(this).attr('src').replace("-d", "-a"));
     $(this).siblings(".tip").css('opacity', '1');
 });

 $('.wrapper-image img').mouseout(function () {
     $(this).attr('src', $(this).attr('src').replace("-a", "-d"));
     $(this).siblings(".tip").css('opacity', '0');
 });
