  var currentCompositon = 0;
  var currentPlaylist = '';

  function playDefault() {
      console.log("Play default");
      playlist_name = playlists['random'];
      currentPlaylist = 'random';
      setComposition(playlist_name[currentCompositon]);

      $('audio')[0].play();
  }

  $(".disco-front").click(function () {
      if ($(this).hasClass('disco-default'))
          playPlaylist(this);
      else
          stopPlaylist(this);
  });

  function playPlaylist(elem) {
      stopPlaylist($('[data-playlist=\"' + currentPlaylist + '\"]'))
      currentCompositon = 0;
      currentPlaylist = elem.getAttribute('data-playlist');
      playlist_name = playlists[currentPlaylist];

      setComposition(playlist_name[currentCompositon]);

      $(elem).removeClass('disco-default');
      $(elem).addClass('disco-active');
      $('audio')[0].play();
  }

  function stopPlaylist(elem) {
      currentCompositon = 0;
      $(elem).removeClass('disco-active');
      $(elem).addClass('disco-default');
      $('audio')[0].stop();
  }


  $('#next').click(function () {
      console.log("Next composition");
      nextComposition();
  });


  function setComposition(music_name) {
      $('audio').attr('src', "../muse/player/media/" + currentPlaylist + "/" + music_name + "");
      $('.song-name').html(music_name.replace(".mp3", "").replace(/\%_/gi, " "));
  }


  function nextComposition() {
       $('audio')[0].stop();
      currentCompositon++;
      if (currentCompositon == playlist_name.length) {
          currentCompositon = 0;
      }
      setComposition(playlist_name[currentCompositon]);
      $('audio')[0].play();

  }

  function beforeComposition() {
      currentCompositon--;
      if (currentCompositon == -1) {
          currentCompositon = playlist_name.length - 1;
      }
      setComposition(playlist_name[currentCompositon]);
      $('audio')[0].play();
  }
