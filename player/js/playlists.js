var showbiz =["Muse - Cave.mp3", "Muse - Escapre.mp3", "Muse - Falling Down.mp3", "Muse - Fillip.mp3","Muse - Hate This & I'll Love You.mp3", "Muse - Muscle Museum.mp3", "Muse - Overdue.mp3", "Muse - Showbiz.mp3", "Muse - Sober.mp3", "Muse - Spiral Static.mp3", "Muse - Sunburn.mp3", "Muse - Unintended.mp3", "Muse - Uno.mp3"];

var oss = ["Muse - Bliss.mp3", "Muse - Citizen Erased.mp3", "Muse - Dark Shines.mp3", "Muse - Darkshines.mp3", "Muse - Feeling Good.mp3", "Muse - Futurism.mp3", "Muse - Hyper Music.mp3", "Muse - Megalomania.mp3", "Muse - Micro Cuts.mp3", "Muse - New Born.mp3", "Muse - Plug In Baby.mp3", "Muse - Screenager.mp3", "Muse - Space Dementia.mp3"];

var absolution = ["Muse - Apocalypse Please.mp3","Muse - Blackout.mp3","Muse - Butterflies & Hurricanes.mp3","Muse - Endlessly.mp3","Muse - Falling Away With You.mp3","Muse - Fury.mp3","Muse - Hysteria.mp3","Muse - Interlude.mp3","Muse - Intro.mp3","Muse - Ruled by Secrecy.mp3","Muse - Sing for Absolution.mp3","Muse - Stockholm Syndrome.mp3","Muse - The Small Print.mp3","Muse - Thoughts of a Dying Atheist.mp3","Muse - Time Is Running Out.mp3"];

var bhar = ["Muse - Assassin.mp3","Muse - City of Delusion.mp3","Muse - Exo-Politics.mp3","Muse - Glorious.mp3","Muse - Hoodoo.mp3","Muse - Invincible.mp3","Muse - Knights of Cydonia.mp3","Muse - Map of the Problematique.mp3","Muse - Soldier's Poem.mp3","Muse - Starlight.mp3","Muse - Supermassive Black Hole.mp3","Muse - Take a Bow.mp3"];

var t2l =["Muse - Animals.mp3", "Muse - Big Freeze.mp3", "Muse - Explorers.mp3", "Muse - Follow Me.mp3", "Muse - Liquid State.mp3", "Muse - Madness.mp3", "Muse - Panic Station.mp3", "Muse - Prelude.mp3", "Muse - Save Me.mp3", "Muse - Supremacy.mp3", "Muse - Survival.mp3", "Muse - The 2nd Law Isolated System.mp3", "Muse - The 2nd Law Unsustainable.mp3"];

var tr = ["Muse - Exogenesis_ Symphony Part I (Overture).mp3", "Muse - Exogenesis_ Symphony Part II (Cross Pollination).mp3", "Muse - Exogenesis_ Symphony Part III (Redemption).mp3", "Muse - Guiding Light.mp3", "Muse - I Belong to You (+Mon Coeur S'ouvre a Ta Voix).mp3", "Muse - MK ULTRA.mp3", "Muse - Resistance.mp3", "Muse - Undisclosed Desires.mp3", "Muse - United States of Eurasia (+Collateral Damage).mp3", "Muse - Unnatural Selection.mp3", "Muse - Uprising.mp3"];

var random = ["Muse%_-%_Exo-Politics.mp3", "Muse%_-%_Feeling%_Good.mp3", "Muse%_-%_Fury.mp3", "Muse%_-%_Hoodoo.mp3", "Muse%_-%_Hyper Music.mp3", "Muse%_-%_Knights%_of%_Cydonia.mp3", "Muse%_-%_Plug%_In%_Baby.mp3", "Muse%_-%_Sing%_for%_ Absolution.mp3","Muse%_-%_Thoughts%_of%_a%_Dying%_Atheist.mp3"];

var playlists = new Object ();

playlists['showbiz'] = showbiz;
playlists['oss'] = oss;
playlists['absolution'] = absolution;
playlists['bhar'] = bhar;
playlists['t2l'] = t2l;
playlists['tr'] = tr;
playlists['random'] = random;

